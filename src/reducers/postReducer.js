import { FETCH_TV_SHOWS, FETCH_EPISODES, GET_SHOW } from "../actions/types";

export const initialState = {
  show: {},
  episodes: {}
};

export const getShow = (state = initialState, action) => {
  switch (action.type) {
    case GET_SHOW:
      return {
        ...state,
        loading: true
      };
    case FETCH_TV_SHOWS:
      return {
        ...state,
        loading: false,
        show: action.data
      };
    case FETCH_EPISODES:
      return {
        ...state,
        loading: false,
        episodes: action.data
      };

    default:
      return state;
  }
};
