import { combineReducers } from "redux";
import { getShow } from "./postReducer";

export default combineReducers({
  getShow: getShow
});
