import React from "react";
import {
  EpisodeDetailsContainer,
  ContentBox,
  EpisodePicture,
  Description
} from "./EpisodeDetailsStyle";

export default function EpisodeDetails(data) {
  const episodeData = data.data;
  return (
    <EpisodeDetailsContainer>
      <h1>{episodeData.name}</h1>
      <ContentBox>
        <EpisodePicture
          src={episodeData.image && episodeData.image.medium}
          alt={episodeData.name}
        ></EpisodePicture>
        <Description
          dangerouslySetInnerHTML={{ __html: episodeData.summary }}
        ></Description>
      </ContentBox>
    </EpisodeDetailsContainer>
  );
}
