import React, { Component } from "react";
import { Link } from "react-router-dom";
import EpisodeDetails from "../EpisodeDetails";
import { ButtonContainer } from "./EpisodeStyle";

class Episode extends Component {
  render() {
    const { data } = this.props && this.props.location;
    return (
      <>
        <ButtonContainer>
          <Link to="/">Back to The Powerpuff Girls</Link>
        </ButtonContainer>
        <EpisodeDetails data={data} />
      </>
    );
  }
}

export default Episode;
