import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { getShow } from "../actions/postAction";
import EpisodeDetails from "./EpisodeDetails";
import EpisodeList from "./EpisodeList";

class TvShows extends Component {
  UNSAFE_componentWillMount() {
    this.props.getShow();
  }

  render() {
    const { tvShows, episodes } = this.props;
    return (
      <>
        <EpisodeDetails data={tvShows} />
        {episodes.length > 0 && <EpisodeList data={episodes} />}
      </>
    );
  }
}

TvShows.propTypes = {
  getShow: PropTypes.func.isRequired
};

const mapStateToProps = state => ({
  tvShows: state.getShow.show,
  episodes: state.getShow.episodes
});

const mapDispatchToProps = {
  getShow
};

export default connect(mapStateToProps, mapDispatchToProps)(TvShows);
