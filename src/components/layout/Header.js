import React from "react";
import logo from "./../../logo.svg";
import { HeaderContainer } from "./HeaderStyle";

const Header = () => {
  return (
    <HeaderContainer>
      <img src={logo} alt="logo" />
    </HeaderContainer>
  );
};

export default Header;
