import styled from "styled-components";
import media from "styled-media-query";

export const EpisodeDetailsContainer = styled.div`
  ${media.lessThan("375px")`
    margin: 15px 0 0 0;
  `}
`;

export const Description = styled.div`
  width: 100%;
`;

export const ContentBox = styled.div`
  width: 100%;
`;

export const EpisodePicture = styled.img`
  float: left;
  margin-right: 20px;
  max-width: 180px;
  ${media.lessThan("375px")`
    max-width: 100%;
    width: 100%;
    float: none;
    margin: 0 auto;
  `}
`;
