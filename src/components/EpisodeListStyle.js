import styled from "styled-components";

export const EpisodeListTable = styled.table`
  overflow-y: auto;
  max-height: 500px;
  clear: both;
  thead th {
    position: sticky;
    top: 0;
    background: #282c34;
    font-size: 14px;
    text-align: left;
    padding: 20px 5px 20px 0px;
  }
  tr {
    font-size: 14px;
  }
  a {
    color: #c2fcff;
    text-decoration: none;
    font-size: 14px;
    cursor: pointer;
  }
`;
