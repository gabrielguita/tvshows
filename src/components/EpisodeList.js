import React from "react";
import { Link } from "react-router-dom";
import { EpisodeListTable } from "./EpisodeListStyle";

const EpisodeList = data => {
  const episodesData = data.data;
  const episodeHeader = {
    season: "Season",
    date: "Date",
    episode: "Episode",
    name: "Name"
  };
  return (
    <EpisodeListTable>
      <thead>
        <tr>
          <th>{episodeHeader.season}</th>
          <th>{episodeHeader.date}</th>
          <th>{episodeHeader.episode}</th>
          <th>{episodeHeader.name}</th>
        </tr>
      </thead>
      <tbody>
        {episodesData &&
          episodesData.map(episode => (
            <tr key={episode.id}>
              <td>{episode.season}</td>
              <td>{episode.airdate}</td>
              <td>{episode.number}</td>
              <td>
                <Link
                  className="episodeLink"
                  to={{
                    pathname: `/episode/${episode.name
                      .replace(/\s+/g, "-")
                      .toLowerCase()}`,
                    data: episode
                  }}
                >
                  {episode.name}
                </Link>
              </td>
            </tr>
          ))}
      </tbody>
    </EpisodeListTable>
  );
};

export default EpisodeList;
