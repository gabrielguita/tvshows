import { put, fork, all } from "redux-saga/effects";

const showId = "6771";
const apiUrl = "https://api.tvmaze.com/shows";

function* fetchNews() {
  const data = yield fetch(`${apiUrl}/${showId}`)
    .then(response => response.json())
    .catch(err => console.log(err));

  yield put({ type: "FETCH_TV_SHOWS", data });
}

function* fetchEpisodes() {
  const data = yield fetch(`${apiUrl}/${showId}/episodes`)
    .then(response => response.json())
    .catch(err => console.log(err));

  yield put({ type: "FETCH_EPISODES", data });
}

export default function* rootSaga() {
  yield all([fork(fetchNews), fork(fetchEpisodes)]);
}
