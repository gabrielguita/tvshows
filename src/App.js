import React from "react";
import { BrowserRouter as Router, Route } from "react-router-dom";
import Header from "./components/layout/Header";
import { Provider } from "react-redux";
import TvShows from "./components/TvShows";
import Episode from "./components/pages/Episode";
import store from "./store";
import { AppContainer } from "./AppStyle";

const App = () => {
  return (
    <Router>
      <Provider store={store}>
        <AppContainer>
          <Header />
          <Route exact path="/" render={props => <TvShows />} />
          <Route path="/episode" component={Episode} />
        </AppContainer>
      </Provider>
    </Router>
  );
};

export default App;
