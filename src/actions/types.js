export const FETCH_TV_SHOWS = "FETCH_TV_SHOWS";
export const FETCH_EPISODES = "FETCH_EPISODES";
export const GET_SHOW = "GET_SHOW";
