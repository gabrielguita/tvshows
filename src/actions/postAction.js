import { GET_SHOW } from "./types";

export const getShow = () => ({
  type: GET_SHOW
});
